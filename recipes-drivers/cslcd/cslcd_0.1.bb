SUMMARY = "Chipsee LCD drivers"
DESCRIPTION = "Recipe created by bitbake-layers"
SECTION = "kernel/modules"
PRIORITY = "optional"
LICENSE = "CLOSED"
DEPENDS = "virtual/kernel"

inherit module
SRC_URI = "https://github.com/rohitzsh/chipsee-driver-lcd.git;branch=master;protocol=https"

SRCREV = "09a1f2551b7c50b03551b055c034ac69db93e6ac"
SRC_URI[sha256sum] = "c66c73ae1b74e76d6d025c313a9fdbd04e2fbd4f744c984fcf60adbfc802136b"

S = "${WORKDIR}"
RPROVIDES_${PN} += "kernel-module-cs_lcd"
